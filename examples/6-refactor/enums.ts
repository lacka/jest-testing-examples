export enum CurrencyType {
  EUR = 1,
  GBP = 2,
  USD = 3,
}

export enum TransactionState {
  CURRENT = 1,
  PENDING = 2,
  SUBMITTED = 3,
}
