jest.mock("./api-service.ts", () => ({
  ...jest.requireActual("./api-service.ts"),
  getExchangeRatesOfCurrency: jest.fn().mockResolvedValue({ midRate: 0.5 }),
}));

import { CurrencyType, TransactionState } from "./enums";
import { Processor } from "./refactor";
import * as ApiModule from './api-service';

describe("Process", () => {
  let processor: Processor;

  beforeAll(() => {
    processor = new Processor([
      {
        id: 1,
        name: "EUR",
      },
      {
        id: 2,
        name: "USD",
      },
      {
        id: 3,
        name: "GBP",
      },
    ]);
  });

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should satisfy default", async () => {

    // @ts-expect-error
    (ApiModule.getExchangeRatesOfCurrency as jest.SpyInstance).mockResolvedValue({ midRate: 10 });

    const processed = await processor.process(
      [
        {
          period: "January",
          items: [
            {
              periodName: "January",
              state: TransactionState.CURRENT,
              countryName: "HUN",
              countryId: 1,
              taxAmount: 10,
              currency: CurrencyType.EUR,
            },
            {
              periodName: "January",
              state: TransactionState.CURRENT,
              countryName: "HUN",
              countryId: 1,
              taxAmount: 20,
              currency: CurrencyType.USD,
            },
          ],
        },
      ],
      [
        {
          period: "February",
          items: [
            {
              periodName: "February",
              state: TransactionState.PENDING,
              countryName: "HUN",
              countryId: 1,
              taxAmount: 10,
              currency: CurrencyType.GBP,
            },
          ],
        },
        {
          period: "March",
          items: [
            {
              periodName: "March",
              state: TransactionState.PENDING,
              countryName: "HUN",
              countryId: 1,
              taxAmount: 15,
              currency: CurrencyType.GBP,
            },
          ],
        },
      ],
      [
        {
          period: "March",
          items: [
            {
              periodName: "March",
              state: TransactionState.SUBMITTED,
              countryName: "HUN",
              countryId: 1,
              taxAmount: 10,
              currency: CurrencyType.USD,
            },
            {
              periodName: "March",
              state: TransactionState.SUBMITTED,
              countryName: "HUN",
              countryId: 1,
              taxAmount: 20,
              currency: CurrencyType.GBP,
            },
          ],
        },
      ]
    );

    expect(processed).toStrictEqual({
      GBP: [
        {
          countryId: 1,
          countryName: "HUN",
          currency: 3,
          periodName: "January",
          state: 1,
          taxAmount: 20,
          taxAmountInEUR: 2,
        },
        {
          countryId: 1,
          countryName: "HUN",
          currency: 3,
          periodName: "March",
          state: 3,
          taxAmount: 10,
          taxAmountInEUR: 1,
        },
      ],
      USD: [
        {
          countryId: 1,
          countryName: "HUN",
          currency: 2,
          periodName: "February",
          state: 2,
          taxAmount: 10,
          taxAmountInEUR: 1,
        },
        {
          countryId: 1,
          countryName: "HUN",
          currency: 2,
          periodName: "March",
          state: 2,
          taxAmount: 15,
          taxAmountInEUR: 1.5,
        },
        {
          countryId: 1,
          countryName: "HUN",
          currency: 2,
          periodName: "March",
          state: 3,
          taxAmount: 20,
          taxAmountInEUR: 2,
        },
      ],
    });
  });

  it("should be correct for consecutive calls", async () => {
    const input: [any, any, any] = [[
      {
        period: "January",
        items: [
          {
            periodName: "January",
            state: TransactionState.CURRENT,
            countryName: "HUN",
            countryId: 1,
            taxAmount: 10,
            currency: CurrencyType.EUR,
          },
          {
            periodName: "January",
            state: TransactionState.CURRENT,
            countryName: "HUN",
            countryId: 1,
            taxAmount: 20,
            currency: CurrencyType.USD,
          },
        ],
      },
    ],
    [
      {
        period: "February",
        items: [
          {
            periodName: "February",
            state: TransactionState.PENDING,
            countryName: "HUN",
            countryId: 1,
            taxAmount: 10,
            currency: CurrencyType.GBP,
          },
        ],
      },
      {
        period: "March",
        items: [
          {
            periodName: "March",
            state: TransactionState.PENDING,
            countryName: "HUN",
            countryId: 1,
            taxAmount: 15,
            currency: CurrencyType.GBP,
          },
        ],
      },
    ],
    [
      {
        period: "March",
        items: [
          {
            periodName: "March",
            state: TransactionState.SUBMITTED,
            countryName: "HUN",
            countryId: 1,
            taxAmount: 10,
            currency: CurrencyType.USD,
          },
          {
            periodName: "March",
            state: TransactionState.SUBMITTED,
            countryName: "HUN",
            countryId: 1,
            taxAmount: 20,
            currency: CurrencyType.GBP,
          },
        ],
      },
    ]];
    const processedFirst = await processor.process(...input);
    const processedSecond = await processor.process(...input);

    const expectedOutput = {
      GBP: [
        {
          countryId: 1,
          countryName: "HUN",
          currency: 3,
          periodName: "January",
          state: 1,
          taxAmount: 20,
          taxAmountInEUR: expect.any(Number),
        },
        {
          countryId: 1,
          countryName: "HUN",
          currency: 3,
          periodName: "March",
          state: 3,
          taxAmount: 10,
          taxAmountInEUR: expect.any(Number),
        },
      ],
      USD: [
        {
          countryId: 1,
          countryName: "HUN",
          currency: 2,
          periodName: "February",
          state: 2,
          taxAmount: 10,
          taxAmountInEUR: expect.any(Number),
        },
        {
          countryId: 1,
          countryName: "HUN",
          currency: 2,
          periodName: "March",
          state: 2,
          taxAmount: 15,
          taxAmountInEUR: expect.any(Number),
        },
        {
          countryId: 1,
          countryName: "HUN",
          currency: 2,
          periodName: "March",
          state: 3,
          taxAmount: 20,
          taxAmountInEUR: expect.any(Number),
        },
      ],
    };
    expect(processedFirst).toStrictEqual(expectedOutput);
    expect(processedSecond).toStrictEqual(expectedOutput);
  });

  it("should return empty object no data", async () => {
    const output = await processor.process([],[],[]);

    expect(output).toEqual({});
  });

  it("should thow an error if not called with arrays", async () => {
    await expect(processor.process(undefined, undefined, undefined)).rejects.toThrowError('Invalid Arguments!');
  });

  it("should throw an error if api thows an error", async () => {
    // @ts-expect-error
    (ApiModule.getExchangeRatesOfCurrency as jest.SpyInstance).mockRejectedValueOnce(new Error('Internal Server Error!'));

    const errorProcessor = new Processor([
      {
        id: 1,
        name: "EUR",
      },
      {
        id: 2,
        name: "USD",
      },
      {
        id: 3,
        name: "GBP",
      },
    ]);
    await expect(errorProcessor.process([
      {
        period: "March",
        items: [
          {
            periodName: "March",
            state: TransactionState.SUBMITTED,
            countryName: "HUN",
            countryId: 1,
            taxAmount: 10,
            currency: CurrencyType.USD,
          },
          {
            periodName: "March",
            state: TransactionState.SUBMITTED,
            countryName: "HUN",
            countryId: 1,
            taxAmount: 20,
            currency: CurrencyType.GBP,
          },
        ],
      },
    ], [], [])).rejects.toThrowError('Internal Server Error!');
  });

  it("should call API the right times (All Currencies - EUR)", async () => {


    const apiProcessor = new Processor([
      {
        id: 1,
        name: "EUR",
      },
      {
        id: 2,
        name: "USD",
      },
      {
        id: 3,
        name: "GBP",
      },
    ]);

    await apiProcessor.process([
      {
        period: "March",
        items: [
          {
            periodName: "March",
            state: TransactionState.SUBMITTED,
            countryName: "HUN",
            countryId: 1,
            taxAmount: 10,
            currency: CurrencyType.USD,
          },
          {
            periodName: "March",
            state: TransactionState.SUBMITTED,
            countryName: "HUN",
            countryId: 1,
            taxAmount: 20,
            currency: CurrencyType.GBP,
          },
          {
            periodName: "March",
            state: TransactionState.SUBMITTED,
            countryName: "HUN",
            countryId: 1,
            taxAmount: 22,
            currency: CurrencyType.EUR,
          },
        ],
      },
    ], [], []);

    expect(ApiModule.getExchangeRatesOfCurrency).toHaveBeenCalledTimes(2);
  });

  it("should call API the right times and only once per currecny (All Currencies - EUR)", async () => {
    const apiProcessor = new Processor([
      {
        id: 1,
        name: "EUR",
      },
      {
        id: 2,
        name: "USD",
      },
      {
        id: 3,
        name: "GBP",
      },
    ]);

    await apiProcessor.process([
      {
        period: "March",
        items: [
          {
            periodName: "March",
            state: TransactionState.SUBMITTED,
            countryName: "HUN",
            countryId: 1,
            taxAmount: 10,
            currency: CurrencyType.USD,
          },
          {
            periodName: "March",
            state: TransactionState.SUBMITTED,
            countryName: "HUN",
            countryId: 1,
            taxAmount: 20,
            currency: CurrencyType.GBP,
          },{
            periodName: "March",
            state: TransactionState.SUBMITTED,
            countryName: "HUN",
            countryId: 1,
            taxAmount: 10,
            currency: CurrencyType.USD,
          },
          {
            periodName: "March",
            state: TransactionState.SUBMITTED,
            countryName: "HUN",
            countryId: 1,
            taxAmount: 20,
            currency: CurrencyType.GBP,
          },
          {
            periodName: "March",
            state: TransactionState.SUBMITTED,
            countryName: "HUN",
            countryId: 1,
            taxAmount: 22,
            currency: CurrencyType.EUR,
          },
        ],
      },
    ], [], []);

    expect(ApiModule.getExchangeRatesOfCurrency).toHaveBeenCalledTimes(2);
  });
});
