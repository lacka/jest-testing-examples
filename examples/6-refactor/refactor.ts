import { getExchangeRatesOfCurrency } from "./api-service";
import { CurrencyType, TransactionState } from "./enums";
import { TransactionGroupStored, TransactionStored } from "./interfaces";

export class Processor {
  private _cachedCountryFxRates = new Map();
  private generalCurrencies: Record<string, any> = {};

  public otherGroupPendingPeriods = [];
  public otherGroupSubmittedPeriods = [];
  public otherGroupCurrentPeriods = [];

  constructor(currencies: any[]) {
    currencies.forEach((c) => (this.generalCurrencies[c.id] = c.name));
  }

  async process(current: any[], other: any[], submitted: any[]): Promise<any> {
    if(!Array.isArray(current) || !Array.isArray(other) || !Array.isArray(submitted)) {
      throw new Error('Invalid Arguments!');
    }
  
    const dict = {};
    this.collectRowsByCurrency(current, dict);
    this.collectRowsByCurrency(other, dict);
    this.collectRowsByCurrency(submitted, dict);

    await this.fillExchangeRates(dict);

    return dict;
  }

  private collectRowsByCurrency(groups, dict: { [c: string]: any[] }) {
    groups.forEach((periodGroup) => {
      const rows = periodGroup.items;
      rows
        .filter(
          (r) => r.taxAmount && r.currency && r.currency !== CurrencyType.EUR
        )
        .forEach((r) => {
          let a = dict[this.generalCurrencies[r.currency]];
          if (!a) {
            a = dict[this.generalCurrencies[r.currency]] = [];
          }

          a.push(r);
        });
    });
  }

  private fillExchangeRates(dict: { [currency: string]: any[] }) {
    const currencies = Object.keys(dict);

    return Promise.all(
      currencies.map((currency) => {
        const transactionRows = dict[currency];
        if (this._cachedCountryFxRates.get(currency)) {
          return this._fillTransactionRowEURAmount(
            transactionRows,
            currency,
            null
          );
        } else {
          return getExchangeRatesOfCurrency(currency as any).then(
            (fxRate: any) => {
              this._fillTransactionRowEURAmount(
                transactionRows,
                currency,
                fxRate
              );
            }
          );
        }
      })
    );
  }

  private _fillTransactionRowEURAmount(
    transactionRows: any[],
    currency: string,
    apiFxRate: any
  ) {
    for (const row of transactionRows) {
      const fxRate = apiFxRate
        ? apiFxRate
        : this._cachedCountryFxRates.get(currency);
      row.taxAmountInEUR =
        fxRate && fxRate.midRate
          ? Math.round((row.taxAmount / fxRate.midRate) * 100) / 100
          : null;
      if (!this._cachedCountryFxRates.get(currency)) {
        this._cachedCountryFxRates.set(currency, fxRate);
      }
    }
  }
}
