import { CurrencyType } from "./enums";

export const getExchangeRatesOfCurrency = async (currency: string) => {
  return new Promise((res, rej) => {
    setTimeout(() => {
      if (Math.random() > 0.8) {
        rej("Internal server error");
      } else {
        res({
          midRate:
            currency === CurrencyType[CurrencyType.EUR]
              ? 1
              : currency === CurrencyType[CurrencyType.USD]
              ? 0.9
              : 0.8,
        });
      }
    }, 4000);
  });
};
