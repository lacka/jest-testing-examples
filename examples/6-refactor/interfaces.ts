import { CurrencyType, TransactionState } from "./enums";

export interface Transaction {
  periodName: string;
  state: TransactionState;
  countryName: string;
  countryId: number;
  taxAmount: number;
  currency: CurrencyType;
}

export interface TransactionStored extends Transaction {
  id: any;
  taxAmountInEUR?: number;
}

export interface TransactionGroupStored {
  id: any;
  period: string;
  items: TransactionStored[];
}
