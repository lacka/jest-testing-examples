import { calcError, getUser } from "./utils";

describe("Error", () => {
  it("should expect error from sync", () => {
    expect(() => calcError()).toThrow();
  });

  it("should expect error from async", async () => {
    await expect(getUser(100)).rejects.toThrow(`User not found with id: 100`);
  });
});
