export const getUser = async (id: number) => {
  throw new Error(`User not found with id: ${id}`);
};

export const calcError = () => {
  throw new Error("Error");
};
