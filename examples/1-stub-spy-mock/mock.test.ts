jest.mock("./api-example.ts", () => ({
  ...jest.requireActual("./api-example.ts"),
  getRandom: () => Promise.resolve(42),
}));

import { getUserWithExpensiveRandomNumber } from "./stub-spy-mock";

describe("Test async methods with long run", () => {
  afterAll(() => {
    jest.clearAllMocks();
  });

  it("should get user from API with id 30 and mocked random", (done: Function) => {
    getUserWithExpensiveRandomNumber(30).then((user) => {
      expect(user.userId).toBe(30);
      expect(user.name).toBe(`User 30`);
      expect(user.random).toBe(42);
      done();
    });
  });

  it("should get user from API with id 40 and mocked random", async () => {
    const user = await getUserWithExpensiveRandomNumber(40);
    expect(user.userId).toBe(40);
    expect(user.name).toBe(`User 40`);
    expect(user.random).toBe(42);
  });
});
