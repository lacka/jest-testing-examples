jest.mock("./api-example.ts", () => ({
  ...jest.requireActual("./api-example.ts"),
  getRandom: () => Promise.resolve(42),
}));

import {
  add,
  getUserWithExpensiveRandomNumber,
  getUserWithRandomNumber,
} from "./stub-spy-mock";

describe("Testing sync methods", () => {
  it("should 2 + 2 equals 4", () => {
    expect(add(2, 2)).toBe(4);
  });

  it("should 2 + 3 not equals 4", () => {
    expect(add(2, 3)).not.toBe(4);
  });
});

describe("Test async methods", () => {
  it("should get user from API with id 10", (done: Function) => {
    getUserWithRandomNumber(10).then((user) => {
      expect(user.userId).toBe(10);
      expect(user.name).toBe(`User 10`);
      done();
    });
  });

  it("should get user from API with id 20", async () => {
    const user = await getUserWithRandomNumber(20);
    expect(user.userId).toBe(20);
    expect(user.name).toBe(`User 20`);
  });
});

describe("Test async methods with long run", () => {
  afterAll(() => {
    jest.clearAllMocks();
  });

  it("should get user from API with id 30 and mocked random", (done: Function) => {
    getUserWithExpensiveRandomNumber(30).then((user) => {
      expect(user.userId).toBe(30);
      expect(user.name).toBe(`User 30`);
      expect(user.random).toBe(42);
      done();
    });
  });

  it("should get user from API with id 40 and mocked random", async () => {
    const user = await getUserWithExpensiveRandomNumber(40);
    expect(user.userId).toBe(40);
    expect(user.name).toBe(`User 40`);
    expect(user.random).toBe(42);
  });
});
