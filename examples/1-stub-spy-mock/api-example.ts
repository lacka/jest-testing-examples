export const getRandom = async (timeout = 300) => {
  return new Promise((res) => {
    setTimeout(() => {
      res(Math.random());
    }, timeout);
  });
};
