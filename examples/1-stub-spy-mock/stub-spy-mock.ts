import { getRandom } from "./api-example";

export const add = (a: number, b: number) => {
  return a + b;
};

export const getUserWithRandomNumber = async (userId: number) => {
  const random = await getRandom();
  return {
    userId,
    name: `User ${userId}`,
    random,
  };
};

export const getUserWithExpensiveRandomNumber = async (userId: number) => {
  const random = await getRandom(6000);
  return {
    userId,
    name: `User ${userId}`,
    random,
  };
};
