export const Stack = () => {
  const _stack = [];

  return {
    push: (value: number) => {
      const index = _stack.push(value);
      return _stack[index];
    },
    pull: () => {
      return _stack[_stack.length - 1];
    },
    pop: () => {
      return _stack.pop();
    },
  };
};
