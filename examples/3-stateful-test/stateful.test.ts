import { Stack } from "./stack";

describe("Stack with local init", () => {
  it("should add number to stack", () => {
    const stack = Stack();
    stack.push(1);
    expect(stack.pull()).toBe(1);
  });

  it("should add 2 number to stack", () => {
    const stack = Stack();
    stack.push(1);
    stack.push(2);
    expect(stack.pull()).toBe(2);
  });

  it("should add 2 number and remove 1 from stack", () => {
    const stack = Stack();
    stack.push(3);
    stack.push(4);
    const last = stack.pop();
    expect(last).toBe(4);
    expect(stack.pull()).toBe(3);
  });
});

describe("Stack with hook init", () => {
  let stack;

  beforeEach(() => (stack = Stack()));
  afterEach(() => (stack = null));

  it("should add number to stack", () => {
    stack.push(1);
    expect(stack.pull()).toBe(1);
  });

  it("should add 2 number to stack", () => {
    stack.push(1);
    stack.push(2);
    expect(stack.pull()).toBe(2);
  });

  it("should add 2 number and remove 1 from stack", () => {
    stack.push(3);
    stack.push(4);
    const last = stack.pop();
    expect(last).toBe(4);
    expect(stack.pull()).toBe(3);
  });
});

describe("Stack with global init", () => {
  let stack = Stack();

  it("should add numbers to stack", () => {
    stack.push(10);
    stack.push(20);
    stack.push(30);
    stack.push(40);
    expect(stack.pull()).toBe(40);
  });

  it("should get last which is 40", () => {
    expect(stack.pull()).toBe(40);
  });

  it("should remove last and top is 30", () => {
    stack.pop();
    expect(stack.pull()).toBe(30);
  });

  it("should remove last and top is 20, popped is 30", () => {
    const last = stack.pop();
    expect(last).toBe(30);
    expect(stack.pull()).toBe(20);
  });
});
