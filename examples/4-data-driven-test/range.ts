export const isInRange = (target, rangeMin, rangeMax) => {
    return target >= rangeMin && target <= rangeMax;
}