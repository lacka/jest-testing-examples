import { isInRange } from "./range";

describe('isInRange', () => {
    const tests = [
        {
            name: 'should work with regular numbers',
            args: [2, 1, 300],
            output: true,
        },
        {
            name: 'should detect if target exceeds max',
            args: [2020, 1, 300],
            output: false,
        },
        {
            name: 'should detect if target exceeds min',
            args: [0, 1, 300],
            output: false,
        },
        {
            name: 'should not work with inverted range',
            args: [2, 300, 1],
            output: false,
        },
        {
            name: 'should not work with string',
            args: ['a', 1, 300],
            output: false,
        },
    ];

    tests.forEach(test => {
        it(test.name, () => {
            expect(isInRange.apply(null, test.args)).toEqual(test.output);
        });
    });
});