export const logger = (msg: string) => {
  console.log(msg);
};

export const delayedLog = (l = logger) => {
  return setTimeout(() => l("called"), 2000);
};

export const intervaledLog = (l = logger) => {
  return setInterval(() => l("called"), 2000);
};
