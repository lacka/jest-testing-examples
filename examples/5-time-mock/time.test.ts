import { delayedLog, intervaledLog } from "./timed";

describe("Timers", () => {
  it("should not call log", () => {
    const log = jest.fn();
    const timer = delayedLog(log);
    expect(log).toBeCalledTimes(0);
    clearTimeout(timer);
  });

  it("should call log once", () => {
    jest.useFakeTimers();
    const log = jest.fn();
    const timer = delayedLog(log);
    jest.advanceTimersByTime(2500);
    expect(log).toBeCalledTimes(1);
    clearTimeout(timer);
    jest.useRealTimers();
  });

  it("should call log 3 times", () => {
    jest.useFakeTimers();
    const log = jest.fn();
    const timer = intervaledLog(log);
    jest.advanceTimersByTime(6000);
    expect(log).toBeCalledTimes(3);
    clearInterval(timer);
    jest.useRealTimers();
  });

  it("should call log 30 times", () => {
    jest.useFakeTimers();
    const log = jest.fn();
    const timer = intervaledLog(log);
    jest.advanceTimersByTime(60000);
    expect(log).toBeCalledTimes(30);
    clearInterval(timer);
    jest.useRealTimers();
  });
});
